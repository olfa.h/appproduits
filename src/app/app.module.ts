import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListeComponent } from './liste/liste.component';
import { GestionService } from './gestion.service';
import { HttpClientModule} from '@angular/common/http';
import { AjoutComponent } from './ajout/ajout.component';
import { FormsModule} from '@angular/forms';
import { DetailComponent } from './detail/detail.component';
import { TestComponent } from './test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    ListeComponent,
    AjoutComponent,
    DetailComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [GestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
