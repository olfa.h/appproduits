import { Component, OnInit } from '@angular/core';
import { GestionService } from '../gestion.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-ajout',
  templateUrl: './ajout.component.html',
  styleUrls: ['./ajout.component.css']
})
export class AjoutComponent implements OnInit {
prod:object = { "description":"","qte":"","prix":""}
  constructor(private service:GestionService, private route:Router) { }

  ngOnInit() {

  }
ajouter(){
this.service.addProduit(this.prod).subscribe(
  data => {this.route.navigate['/produits'] },
  err => {}
)
}
}
