import { Component, OnInit } from '@angular/core';
import { GestionService } from '../gestion.service';

@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css']
})
export class ListeComponent implements OnInit {
prods:any=[]
  constructor(private service:GestionService) { }

  ngOnInit() {
    this.getprods();
  }
getprods(){
  this.service.getProduits().subscribe(
data => {this.prods=data},
erre => {console.log(erre) }


  )
}
supprimer(id:number){
  this.service.deleteProduit(id).subscribe(
    data => {this.ngOnInit()},
    err => { }
  )
}
}