import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GestionService } from '../gestion.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
produit:any={"id" :null, "description":"","qte":"","prix":""}
  id : number;
  constructor(private ac: ActivatedRoute, private serv:GestionService) {
    this.id = this.ac.snapshot.params["id"];
   }

  ngOnInit() {
    this.serv.getDetail(this.id).subscribe(
      data =>{ this.produit=data
     console.log(data)
      },
      err =>{}
    )

    
  }

}
