import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListeComponent } from './liste/liste.component';
import { AjoutComponent } from './ajout/ajout.component';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [
  { path:"", component:ListeComponent },
  { path: 'detail/:id', component:DetailComponent},
  { path:"produits", component:ListeComponent },
  { path:"ajout" , component:AjoutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
