import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GestionService {

  constructor(private http:HttpClient) { }

getProduits(){
  return this.http.get("http://127.0.0.1:8080/produits");
}
addProduit(prod:any){
  return this.http.post("http://127.0.0.1:8080/add",prod)
}
deleteProduit(id:number){
  return this.http.delete("http://127.0.0.1:8080/delete/"+id)
}
getDetail(id:number){
  return this.http.get("http://127.0.0.1:8080/detail/"+id)
}
}
